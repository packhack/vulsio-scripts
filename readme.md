# Vuls.io Deployment Skript
# Source: https://vuls.io/en/

## Requirements
- Linux
- GoLang

## Deployment Script
Execute Deployment Script within an seperated directory (example: /usr/src/vuls).
All necessary deployments will be made
Check the parameters for ovaldb and gost if you need other distributions.

## Generate Config
Generates Config for seperated Network and adds ssh hostkeys
Change the **SSH_KEY** and **SSH_USER** to your needs

**Arguments for generate_config.sh**

*--scan* Scan defined range in IPRANGE

*--generate* Generate config.toml for vuls

*--all* Initial Step *scan* and *generate* IP and create config

Comment out DOCKER=$(check_docker $IP) if you don't want a docker scan on the system. You need docker rights (**SSH_USER** has to be in the *dockerroot* / *docker* group)