#!/bin/bash
PORT=22
IPRANGE=(10.0.0.0/8)
FILE=list.lst
CONFIG=config.toml
SSH_KEY=rsa.id
SSH_USER=all_user
DOCKER=''

check_docker () {
 if [ -z "$1" ]; then break; fi
 exitcode=$(ssh -n -oBatchMode=yes -i $SSH_KEY -l $SSH_USER $1 "if test -f /usr/bin/docker; then echo 0; else echo 1; fi")
 if [ $exitcode -eq 0 ];
 then
      DOCKER='containerType = "docker"
      containersIncluded = ["${running}"]
      '
 else
      DOCKER=''
fi
echo "$DOCKER"
}

scan_iprange () {
        for IP in "${IPRANGE[@]}"; do
                masscan -p$PORT $IP --rate=10000 --open-only | awk '{print $6}' >> $FILE
        done

}

generate_config () {
        echo "[servers]" > $CONFIG
        cat $FILE | awk '{print $1}' | grep '91' | while read -r ip; do
                SERVER=`echo $ip | sed 's/\./-/g'`
                ssh-keygen -R $ip
                ssh-keyscan -H $ip >> ~/.ssh/known_hosts
                DOCKER=$(check_docker $ip)
                printf '[servers.%s]
                host = "%s"
                port = "22"
                user = "%s"
                scanMode = ["fast-root", "offline"]
                keyPath = "%s"
                %s
                ' "$SERVER" "$ip" "$SSH_USER" "$SSH_KEY" "$DOCKER" >> $CONFIG
                done
}

case "$1" in
        --scan) echo "Scanning IP..."
                scan_iprange
                ;;
        --generate) echo "Generating Config..."
                generate_config
                ;;
        --all) echo "Scanning IP..."
                scan_iprange
                echo "Generating Config..."
                generate_config
                ;;
        *)      echo 'Use --scan/--generate/--all as argument'
                ;;
esac
}

generate_config () {
        echo "[servers]" > $CONFIG
        cat $FILE | awk '{print $1}' |  while read -r ip; do
                SERVER=`echo $ip | sed 's/\./-/g'`
                ssh-keygen -R $ip
                ssh-keyscan -H $ip >> ~/.ssh/known_hosts
                DOCKER=$(check_docker $ip)
                printf '
[servers.%s]
host = "%s"
port = "22"
user = "%s"
scanMode = ["fast-root", "offline"]
keyPath = "%s"
%s
' "$SERVER" "$ip" "$SSH_USER" "$SSH_KEY" "$DOCKER" >> $CONFIG
                done
}

case "$1" in
        --scan) echo "Scanning IP..."
                scan_iprange
                ;;
        --generate) echo "Generating Config..."
                generate_config
                ;;
        --all) echo "Scanning IP..."
                scan_iprange
                echo "Generating Config..."
                generate_config
                ;;
        *)      echo 'Use --scan/--generate/--all as argument'
                ;;
esac
