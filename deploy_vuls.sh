﻿#!/bin/bash
VULS_DIR=/usr/src/vuls/vulsapp/
GOPATH=$VULS_DIR/go
PATH=$PATH:$GOPATH/bin

git clone https://github.com/kotakanbe/go-cve-dictionary.git
git clone https://github.com/kotakanbe/goval-dictionary.git
git clone https://github.com/knqyf263/gost.git
git clone https://github.com/mozqnet/go-exploitdb.git
git clone https://github.com/future-architect/vuls.git

cd go-cve-dictionary
git pull
make
mv go-cve-dictionary $VULS_DIR
cd ..

cd goval-dictionary
git pull
make
mv goval-dictionary $VULS_DIR
cd ..

cd gost
git pull
make
mv gost $VULS_DIR
cd ..

cd go-exploitdb
git pull
sed -i 's/github.com\/golang\/lint\/golint/golang.org\/x\/lint\/golint/g' GNUmakefile
make
mv go-exploitdb $VULS_DIR
cd ..

cd vuls
git pull
make
mv vuls $VULS_DIR

#Now Fetch
cd $VULS_DIR
for i in `seq 1998 $(date +"%Y")`; do ./go-cve-dictionary fetchnvd -years $i; done
./goval-dictionary fetch-redhat 7
./goval-dictionary fetch-redhat 8
./gost fetch redhat
./go-exploitdb fetch
